// *****************************************************************************
// LAPLACE_STL Program
// Supporting code file for the GTC 2021 presentation
// " Fluid dynamics on GPUs with C++ Parallel Algorithms:
//   state-of-the-art performance through a hardware-agnostic approach "

// Copyright © 2021 University of Geneva
// Authors: Jonas Latt
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <algorithm>
#include <numeric>
#include <utility>
#include <functional>
#include <execution>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <chrono>
#include <cmath>
#include <cassert>
#include <omp.h>
#include "mpi.h"

using namespace std;
using namespace std::chrono;

const int N = 4096;               // The domain has a N x N resolution.
const double dx = 1. / (double) (N - 1); // Discrete cell size.
const double dt = dx*dx;          // Discrete time step.
const double D = 0.20;            // Diffusion constant.
const double Dconst = D * dt / (dx*dx);  // Precomputed numerical constant.
const double epsilon = 1.e-5;     // Steady-state convergence criterion.
const int output_frequency = 200; // Terminal output intervals as a number of iterations (-1 means no output).
const int image_frequency  = -1;   // Image output intervals as a number of iterations (-1 means no output).

const bool compare_versions = false; // If true, debug mode is on and the code compares a parallel and a non-parallel version of the algorithm.


// Map 2D indices on a linear index.
inline size_t IND (size_t iX, size_t iY) {
    // You should swap iX and iY in the following line if you want to try swapping lines and columns.
    return iY + N * iX;
};

auto sqr = [](double x) { return x * x; };

void write_to_bmp(int N, double const* data, int iter,
                  double minval, double maxval);

// Set the initial and boundary condition.
void initialize(double* u_ptr, double* utmp_ptr, double dx) {
    // Parallel initialization of u.
    for_each(execution::par_unseq, u_ptr, u_ptr + N*N, [u_ptr, dx](double& u) {
            size_t i = &u - u_ptr;
            size_t iX = i / N;
            size_t iY = i % N;
            double x = (double)iX * dx;
            double y = (double)iY * dx;
            u = 0; // Initial value in bulk
            // Left boundary: A sine period.
            if (iX == 0) {
                u = sin(y * 2. * M_PI);
            }
            // Right boundary: A shifted sine period.
            else if (iX == N - 1) {
                u = sin(y * 2. * M_PI + M_PI / 2.);
            }
            // Top and bottom boundary: linear increase
            else if (iY == 0 || iY == N - 1) {
                u = x;
            }
        } );
    // utmp is a copy of u, as they are going to be used interchangeably
    // at even and odd time steps.
    copy(execution::par_unseq, u_ptr, u_ptr + N*N, utmp_ptr);
}

double iterateFullDomain(double* u_ptr, double* utmp_ptr, double Dconst) {
    double l2 = 0.;
    #pragma omp parallel for reduction(+:l2)
    for (int iX = 1; iX < N-1; iX++) {
        for (int iY = 1; iY < N-1; iY++) {
            size_t i = IND(iX, iY);
            utmp_ptr[i] = u_ptr[i] * (1. - 4. * Dconst)
                           + Dconst * ( u_ptr[IND(iX-1, iY)] + u_ptr[IND(iX+1, iY)] +
                                        u_ptr[IND(iX, iY-1)] + u_ptr[IND(iX, iY+1)] );
            l2 += sqr(utmp_ptr[i] - u_ptr[i]);
        }
    }
    return l2;
}

double iterate_piece(double* u_ptr, double* utmp_ptr,
                     double* u_leftColumn, double* u_rightColumn,
                     double Dconst, int N, int NColumnsLocal, int taskId, int numTasks)
{
    // This code is non-period, and the loop excludes the boundary rows and columns.
    // This is why the the MPI tasks which are on the left and on the right start or
    // end with a different index.
    int lowerX = taskId == 0 ? 1 : 0;
    int upperX = taskId == numTasks-1 ? NColumnsLocal - 1 : NColumnsLocal;
    double l2 = 0.;
    // Option 2:
    //#pragma omp target teams distribute parallel for map(to:u_ptr, u_leftColumn, u_rightColumn),\
                                                     //map(tofrom:utmp_tr) reduction(+:l2)
    // Option 1:
    #pragma omp parallel for reduction(+:l2)
    for (int iX = lowerX; iX < upperX; iX++) {
        for (int iY = 1; iY < N-1; iY++) {
            size_t i = IND(iX, iY);
            double u_left = iX > 0 ? u_ptr[IND(iX-1, iY)] : u_leftColumn[iY];
            double u_right = iX < NColumnsLocal - 1 ? u_ptr[IND(iX+1, iY)] : u_rightColumn[iY];
            utmp_ptr[i] = u_ptr[i] * (1. - 4. * Dconst)
                           + Dconst * (   u_right + u_left +
                                          u_ptr[IND(iX, iY-1)] + u_ptr[IND(iX, iY+1)] );
            l2 += sqr(utmp_ptr[i] - u_ptr[i]);
        }
    }
    return l2;
}

void distribute_initial_condition(double* u, double* u_piece, double* utmp_piece, int N, int NColumnsLocal, int numTasks)
{
    int* counts_send = new int[numTasks];
    int* displacements = new int[numTasks];
    int current_index = 0;
    for (int i = 0; i < numTasks; ++i) {
        counts_send[i] = NColumnsLocal * N;
        displacements[i] = current_index;
        current_index += NColumnsLocal * N;
    }
    MPI_Scatterv(u, counts_send, displacements, MPI_DOUBLE, u_piece,
            NColumnsLocal * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(u, counts_send, displacements, MPI_DOUBLE, utmp_piece,
            NColumnsLocal * N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    delete [] counts_send;
    delete [] displacements;
}


void communicate_columns(double* u_piece, double* leftColumn, double* rightColumn, int N, int NColumnsLocal, int taskId, int numTasks)
{
    int tag = 0;
    MPI_Status status;
    if (taskId > 0) {
        double* receive_from_left = new double[N];
        MPI_Sendrecv(u_piece, N, MPI_DOUBLE, taskId - 1, tag,
                     receive_from_left, N, MPI_DOUBLE, taskId - 1, tag,
                     MPI_COMM_WORLD, &status);
        for (int iY = 0; iY < N; ++iY) {
            leftColumn[iY] = receive_from_left[iY];
        }
        delete [] receive_from_left;
    }
    if (taskId < numTasks - 1) {
        double* receive_from_right = new double[N];
        MPI_Sendrecv(u_piece + N * (NColumnsLocal - 1), N, MPI_DOUBLE, taskId + 1, tag,
                     receive_from_right, N, MPI_DOUBLE, taskId + 1, tag,
                     MPI_COMM_WORLD, &status);
        for (int iY = 0; iY < N; ++iY) {
            rightColumn[iY] = receive_from_right[iY];
        }
        delete [] receive_from_right;
    }
}


int main(int argc, char* argv[])
{
    //mpirun -np 2 -bind-to socket -map-by socket ./heatEquation
    //https://www.archer.ac.uk/training/course-material/2019/06/AdvOpenMP-manch/L10-OpenMPTargetOffload.pdf

    // I think that (maybe) this special version of MPI_Init is needed to run a hybrid MPI / multi-threaded code.
    int provided_guarantee = MPI_THREAD_FUNNELED;
    int ok1 = MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided_guarantee);

    int numTasks, taskId;
    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskId);

    // On task 0, we allocate the full matrix for pre- and post-processing.
    double* u = 0; // u are the degrees of freedom ("the temperature")
    double* utmp = 0;  // we need a temporary matrix because the algorithm is not in place
    if (taskId == 0) {
        u = new double[N * N];
        utmp = new double[N * N];
    }

    // This is a simple code. It only works if the piece assigned to all tasks has exactly the same size.
    assert( N % numTasks == 0 );
    // Number of columns assigned to the local MPI task.
    int NColumnsLocal = N / numTasks; 
    double* u_piece; // A local matrix with "NColumnsLocal" columns.
    double* utmp_piece; // The temporary copy of the lcoal matrix.
    // The communication layers (a column on the left and one on the right) are
    // allocated separately, we are not simply extending u_piece by two columns.
    // In this way, the algorithm runs both for a row-major and a column-major code.
    double *leftColumn, *rightColumn;

    u_piece = new double[N * NColumnsLocal];
    utmp_piece = new double[N * NColumnsLocal];
    leftColumn = new double[N];
    rightColumn = new double[N];

    // Create initial condition on MPI task 0.
    if (taskId == 0) {
        initialize(u, utmp, dx);
    }

    // Then, dispatch the data to the MPI tasks.
    distribute_initial_condition(u, u_piece, utmp_piece, N, NColumnsLocal, numTasks);

    int num_iter = 0;
    int num_image = 0;
    double l2 = 1.;
    auto start = high_resolution_clock::now();
    // Something like the directive below is needed if we use OpenMP and want
    // the memory to be persistent on the GPU.
    //#pragma omp target teams distribute parallel for map(to:u_ptr, u_leftColumn, u_rightColumn),\
                                                     //map(tofrom:utmp_tr) reduction(+:l2)
    while(l2 > epsilon) {
        // In "compare" mode, we run a non-parallel version of the code.
        if (taskId == 0 && compare_versions == true) {
            l2 = iterateFullDomain(u, utmp, Dconst);
        }

        // Update the ghost cells (the left and the right column).
        communicate_columns(u_piece, leftColumn, rightColumn, N, NColumnsLocal, taskId, numTasks);

        // Run the computation on the local slice.
        double l2_local =
            iterate_piece(u_piece, utmp_piece, leftColumn, rightColumn,
                          Dconst, N, NColumnsLocal, taskId, numTasks);

        // Execute a reduction to compute the l2-norm error.
        double l2_parallel;
        MPI_Reduce(&l2_local, &l2_parallel, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        if (taskId == 0) {
            if (compare_versions == true) {
                cout << "l2 = " << l2 << ", l2_parallel=" << l2_parallel << endl;
            }
            else {
                cout << "l2 = " << l2_parallel << endl;
            }
        }

        num_iter++;

        if (taskId == 0 && compare_versions == true) {
            swap(u, utmp);
        }

        swap(u_piece, utmp_piece);

        if (taskId == 0) {
            if (output_frequency != -1 && num_iter % output_frequency == 0) {
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<microseconds>(stop - start);
                double giga_flops = (double)(N*N) * (double)num_iter * 9. / (double)duration.count() * 1.e-3;
                cout << "At step " << num_iter << ", l2 = " << setprecision(5) << sqrt(l2)
                     << "; performance = " << giga_flops << " GFLOPS" << endl;
            }
            if (image_frequency != -1 && num_iter % image_frequency == 0) {
                write_to_bmp(N, u, num_image++, -1., 1.);
            }
        }
    } 

    return 0;
}


// Write the data to an image in the raw BMP format.
// To convert images to a video: ffmpeg -r 40 -i T_%04d.bmp -q:v 0 heat.mp4
void write_to_bmp(int N, double const* data, int iter, double minval, double maxval)
{
    unsigned char bmpfileheader[14] = {'B','M',0,0,0,0,0,0,0,0,54,0,0,0};
    unsigned char bmpinfoheader[40] = { 40, 0,  0, 0,
                                0, 0,  0, 0,
                                0, 0,  0, 0,
                                1, 0, 24, 0 };

    int width = N;
    int height = N;

    int padding = (4 - (width * 3) % 4) % 4;

    int datasize = (3 * width + padding) * height;
    int filesize = 54 + datasize;

    vector<unsigned char> img(datasize);

    // A linear interpolation function, used to create the color scheme.
    auto linear = [](double x, double x1, double x2, double y1, double y2)
       {
           return ( (y2-y1) * x + x2*y1 - x1*y2 ) / (x2-x1);
       };

    for(int iX = 0; iX < width; iX++){
        for(int iY = 0; iY < height; iY++){
            // Restrain the value to be plotted to [0, 1]
            double value = ((data[iY + height * iX] - minval) / (maxval - minval));
            double r = 0., g = 0., b = 0.;
            // For good visibility, use a color scheme that goes from black-blue to black-red.
            if (value <= 1./8.) {
                r = 0.;
                g = 0.;
                b = linear(value, -1./8., 1./8., 0., 1.);
            }
            else if (value <= 3./8.) {
                r = 0.;
                g = linear(value, 1./8., 3./8., 0., 1.);
                b = 1.;
            }
            else if (value <= 5./8.) {
                r = linear(value, 3./8., 5./8., 0., 1.);
                g = 1.;
                b = linear(value, 3./8., 5./8., 1., 0.);
            }
            else if (value <= 7./8.) {
                r = 1.;
                g = linear(value, 5./8., 7./8., 1., 0.);
                b = 0.;
            }
            else {
                r = linear(value, 7./8., 9./8., 1., 0.);
                g = 0.;
                b = 0.;
            }

            r = min(255. * r, 255.);
            g = min(255. * g, 255.);
            b = min(255. * b, 255.);

            img[(iX + iY*width)*3 + iY*padding + 2] = (unsigned char)(r);
            img[(iX + iY*width)*3 + iY*padding + 1] = (unsigned char)(g);
            img[(iX + iY*width)*3 + iY*padding + 0] = (unsigned char)(b);
        }
    }

    bmpfileheader[ 2] = (unsigned char)(filesize      );
    bmpfileheader[ 3] = (unsigned char)(filesize >>  8);
    bmpfileheader[ 4] = (unsigned char)(filesize >> 16);
    bmpfileheader[ 5] = (unsigned char)(filesize >> 24);

    bmpinfoheader[ 4] = (unsigned char)(       width      );
    bmpinfoheader[ 5] = (unsigned char)(       width >>  8);
    bmpinfoheader[ 6] = (unsigned char)(       width >> 16);
    bmpinfoheader[ 7] = (unsigned char)(       width >> 24);
    bmpinfoheader[ 8] = (unsigned char)(       height      );
    bmpinfoheader[ 9] = (unsigned char)(       height >>  8);
    bmpinfoheader[10] = (unsigned char)(       height >> 16);
    bmpinfoheader[11] = (unsigned char)(       height >> 24);
    bmpinfoheader[20] = (unsigned char)(datasize      );
    bmpinfoheader[21] = (unsigned char)(datasize >>  8);
    bmpinfoheader[22] = (unsigned char)(datasize >> 16);
    bmpinfoheader[23] = (unsigned char)(datasize >> 24);

    stringstream filename;
    filename << "T_";
    filename << setfill('0') << setw(4) << iter;
    filename << ".bmp";

    ofstream f;
    f.open(filename.str().c_str(), ios::binary | ios::out);

    f.write((const char*)bmpfileheader, 14);
    f.write((const char*)bmpinfoheader, 40);

    f.write((const char*)&img[0], datasize);
    f.close();
}
