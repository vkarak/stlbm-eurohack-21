#!/usr/bin/env bash

module load cdt-cuda/21.05
module switch PrgEnv-cray PrgEnv-nvidia
module use /apps/daint/UES/eurohack/modules/all
module swap nvidia/21.3 nvidia/21.7
make all
